import java.util.Random;

public class Monster {
	private int hp;
	private int maxHp;
	private int str;
	private int exp;
	private String name;

	public Monster(String name, int hp, int str, int exp) {
		this.name = name;
		this.hp = hp;
		this.str = str;
		this.exp = exp;
		maxHp = hp;
	}

	public int getHp() {
		return hp;
	}

	public int getStr() {

		return str;
	}

	public int getmaxHp() {
		return maxHp;
	}

	public int getExp() {
		return exp;
	}

	public String getName() {
		return name;
	}

	public void setStr(int str) {
		this.str = str;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public int attack() {
		int x;
		Random r = new Random();
		x = r.nextInt(5) + 7;
		x = x + str;
		return x;
	}

}
