import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JButton;

import javax.swing.JOptionPane;

public class Rest {
	private JButton rest = new JButton("Rest");
	private JButton goback = new JButton("Go Back");

	public void playerRest(final Player player1, final Frame jf , final Backpack bag) {

		jf.setCenter("<html>"  + player1.getName() + " Hp: " + player1.getHp()
				+ "/" + player1.getmaxHp() + "<br></br>" + "To rest will cost you 40 gold! " + "<br></br>"
				+ "You have " + player1.getGold() + " gold!"+"<br></br>"
				+ "There is a chanse of getting robbed also!" + "</html>");
		jf.addP(rest);
		jf.addP(goback);

		rest.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (player1.getGold() >= 40) {

					player1.setGold(player1.getGold() - 40);

					Random r = new Random();
					int x = r.nextInt(10) + 1;
					if (x <= 5) {
						JOptionPane
								.showMessageDialog(null,
										"You hade a good night sleep and gain full hp!");
						player1.setHp(player1.getmaxHp());
					} else if (x <= 7 && x >= 6) {
						int j = r.nextInt(15) + 10;
						if (player1.getGold() > j) {
							JOptionPane.showMessageDialog(null,
									"You woke up and notice you been rubbed for "
											+ j + " gold!");
							player1.setGold(player1.getGold() - j);
						} else {
							JOptionPane.showMessageDialog(null,
									"You woke up and notice someone stole you last "
											+ player1.getGold() + " gold!");
							player1.setGold(0);
						}
						player1.setHp(player1.getmaxHp());
					} else if (x <= 9 && x >= 8) {
						int j = r.nextInt(15) + 20;
						if (player1.getGold() > j) {
							JOptionPane.showMessageDialog(null,
									"You woke up and notice you been rubbed for "
											+ j + " gold!");
							player1.setGold(player1.getGold() - j);
						} else {
							JOptionPane.showMessageDialog(null,
									"You woke up and notice someone stole you last "
											+ player1.getGold() + " gold!");
							player1.setGold(0);
						}
						player1.setHp(player1.getmaxHp());
					} else {
						Item i = new Item();
						if (player1.getItem() != 0) {
							JOptionPane.showMessageDialog(null,
									"You woke up and notice someone stole your "
											+ i.lookupitem(player1.getItem()));
							player1.setItem(0);
							
						} else {
							JOptionPane
									.showMessageDialog(null,
											"You hade a good night sleep and gain full hp!");
						}
						player1.setHp(player1.getmaxHp());
					}
					jf.reset();
					MainMenu m = new MainMenu();
					m.StartMenu(player1, jf, bag);

				} else {
					jf.reset();
					JOptionPane.showMessageDialog(null,
							"You dont have money to rest!");
					MainMenu m = new MainMenu();
					m.StartMenu(player1, jf, bag);
				}
			}
		});

		goback.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MainMenu m = new MainMenu();
				m.StartMenu(player1, jf, bag);
			}
		});

	}
}
