import javax.swing.JFrame;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Loot {
	private JFrame jf = new JFrame();

	public int getGold() {
		int x;
		Random r = new Random();
		x = r.nextInt(15) + 35;
		return x;
	}

	public int getItem(Player player1) {

		int x;
		Random r = new Random();
		x = r.nextInt(10) + 1;
		if (x <=4 ) {
			x = 0;
			return x;
		} else if (x<=7 && x>=5) {
			player1.setPotion(player1.getPotion() + 1);
			x = 1;
			return x;
		} else if (x<=9 && x>=8) {
			x = 2;
			return x;
		} else
			x = 3;
		return x;

	}

	public void useItem(final Player player1, final int item, final Backpack bag) {
		Item i = new Item();
		JPanel layout = new JPanel();
		JButton use = new JButton("Use item");
		JButton dont = new JButton("Dont use item");
		JLabel centertext = new JLabel();
		jf.setVisible(true);
		jf.setSize(300, 200);
		jf.setLocation(700, 400);
		layout.add(use);
		layout.add(dont);
		jf.add(centertext, BorderLayout.CENTER);
		jf.add(layout, BorderLayout.SOUTH);
		centertext.setText("<html>Do you like to use the " + i.lookupitem(item)
				+ " that will add " + i.itemdmg(item) + " damage insted of "
				+ i.lookupitem(player1.getItem()) + " that adds "
				+ i.itemdmg(player1.getItem()));
		use.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				bag.addItem(player1.getItem());
				player1.setItem(item);
				jf.dispose();
			}
		});
		dont.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jf.dispose();
			}
		});

	}
}
