import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class Frame {
	private JFrame jf = new JFrame();
	private JPanel button = new JPanel();
	private JLabel toptext = new JLabel("");
	private JLabel centertext = new JLabel("");
	private JLabel west = new JLabel();
	private JLabel east = new JLabel();

	public Frame() {
		jf.setTitle("Quest of the monkey");
		jf.setVisible(true);
		jf.setLocation(400, 250);
		jf.setSize(1000, 600);
		toptext.setHorizontalAlignment(SwingConstants.CENTER);
		centertext.setHorizontalAlignment(SwingConstants.CENTER);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.add(toptext, BorderLayout.NORTH);
		jf.add(centertext, BorderLayout.CENTER);
		jf.add(button, BorderLayout.SOUTH);
		jf.add(west, BorderLayout.WEST);
		jf.add(east, BorderLayout.EAST);

	}

	public void setsize(int x, int y) {
		jf.setSize(x, y);
	}

	public void setlocation(int x, int y) {
		jf.setLocation(x, y);
	}

	public void setTop(String x) {
		toptext.setText(x);
	}

	public void setCenter(String x) {
		centertext.setText(x);
	}

	public void setWest(String x) {
		west.setIcon(new javax.swing.ImageIcon(getClass().getResource(x)));
	}

	public void setEast(String x) {
		east.setIcon(new javax.swing.ImageIcon(getClass().getResource(x)));
	}

	public void addP(JButton x) {
		button.add(x);
	}

	public void addn(JTextField name) {
		jf.add(name, BorderLayout.CENTER);
	}

	public void setName(JTextField name) {
		jf.add(name, BorderLayout.CENTER);
	}

	// public String getName(){
	// return name.getText();
	// }

	public void reset() {
		button.removeAll();
		button.revalidate();
		button.repaint();
		centertext.setText("   ");
		toptext.setText("  ");
	}

}
