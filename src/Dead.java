import javax.swing.JOptionPane;

public class Dead {
	public void die(Player player1) {
		if (player1.getHp() < 1) {
			JOptionPane.showMessageDialog(null,
					"The monster killed you! GAME OVER!");
			System.exit(0);
		}
	}
}
