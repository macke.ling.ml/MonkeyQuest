import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class SaveLoad {

	public void load(Player player1,Backpack bag) throws FileNotFoundException, IOException {
		FileInputStream load = new FileInputStream("C://temp//t.tmp");
		ObjectInputStream ois = new ObjectInputStream(load);
		player1.setName(ois.readUTF());
		player1.setHp(ois.readInt());
		player1.setStr(ois.readInt());
		player1.setnextLevel(ois.readInt());
		player1.setLevel(ois.readInt());
		player1.setExp(ois.readInt());
		player1.setmaxHp(ois.readInt());
		player1.setItem(ois.readInt());
		player1.setGold(ois.readInt());
		player1.setPotion(ois.readInt());
		bag.setBp0(ois.readInt());
		bag.setBp1(ois.readInt());
		bag.setBp2(ois.readInt());
		bag.setBp3(ois.readInt());
		bag.setBp4(ois.readInt());
		bag.setBp5(ois.readInt());
		bag.setBp6(ois.readInt());
		bag.setBp7(ois.readInt());
		bag.setBp8(ois.readInt());
		bag.setBp9(ois.readInt());
		ois.close();

	}

	public void save(Player player1,Backpack bag) throws FileNotFoundException, IOException {
		File map = new File("C://temp//");
		map.mkdir();
		FileOutputStream save = new FileOutputStream("C://temp//t.tmp");
		ObjectOutputStream oos = new ObjectOutputStream(save);
		oos.writeUTF(player1.getName());
		oos.writeInt(player1.getHp());
		oos.writeInt(player1.getStr());
		oos.writeInt(player1.getnextLevel());
		oos.writeInt(player1.getLevel());
		oos.writeInt(player1.getExp());
		oos.writeInt(player1.getmaxHp());
		oos.writeInt(player1.getItem());
		oos.writeInt(player1.getGold());
		oos.writeInt(player1.getPotion());
		oos.writeInt(bag.getBp0());
		oos.writeInt(bag.getBp1());
		oos.writeInt(bag.getBp2());
		oos.writeInt(bag.getBp3());
		oos.writeInt(bag.getBp4());
		oos.writeInt(bag.getBp5());
		oos.writeInt(bag.getBp6());
		oos.writeInt(bag.getBp7());
		oos.writeInt(bag.getBp8());
		oos.writeInt(bag.getBp9());
		oos.close();
	}
}
