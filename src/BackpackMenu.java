import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class BackpackMenu {
	
	private JButton use = new JButton("Use item");
	private JButton remove = new JButton("Remove item");
	private JButton goback = new JButton("Go Back");
	private int option;
	public void inventory(final Player player1, final Frame jf,final Backpack bag){
		bag.bpCheck(jf);
		jf.setEast("player.png");
		jf.setWest("player.png");
		jf.addP(use);
		jf.addP(remove);
		jf.addP(goback);

		use.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				option=1;
				Qframe qf=new Qframe();
				qf.frame(player1,jf,bag,option);
				qf.setTop("<html><br></br>What item you want to use<br><br><br></html>");
			}
		});
		remove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				option=2;
				Qframe qf=new Qframe();
				qf.frame(player1,jf,bag,option);
				qf.setTop("<html><br></br>What item you want to remove<br><br><br></html>");
			}
		});
		goback.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MainMenu m = new MainMenu();
				m.StartMenu(player1, jf, bag);
			}
		});

	}
}
