import javax.swing.JOptionPane;


public class Backpack {
	public int[] bp = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	Item whatitem = new Item();
	
	public void changewep(Player player1,Frame jf){
		if(player1.getA()<=bp.length){
			if(player1.getItem()<2 || player1.getItem()== bp[player1.getA()]){
				
				player1.setItem(bp[player1.getA()]);
				bp[player1.getA()]=0;
				player1.setA(0);
				jf.setCenter("<html>" + "Backpack slot 1 = "
						+ whatitem.lookupitem(bp[0]) + "<br></br>"
						+ "Backpack slot 2 = " + whatitem.lookupitem(bp[1])
						+ "<br></br>" + "Backpack slot 3 = "
						+ whatitem.lookupitem(bp[2]) + "<br></br>"
						+ "Backpack slot 4 = " + whatitem.lookupitem(bp[3])
						+ "<br></br>" + "Backpack slot 5 = "
						+ whatitem.lookupitem(bp[4]) + "<br></br>"
						+ "Backpack slot 6 = " + whatitem.lookupitem(bp[5])
						+ "<br></br>" + "Backpack slot 7 = "
						+ whatitem.lookupitem(bp[6]) + "<br></br>"
						+ "Backpack slot 8 = " + whatitem.lookupitem(bp[7])
						+ "<br></br>" + "Backpack slot 9 = "
						+ whatitem.lookupitem(bp[8]) + "<br></br>"
						+ "Backpack slot 10 = " + whatitem.lookupitem(bp[9]));
			}else{
				jf.setCenter("<html>You have better or the same weapon on you!</html>");
			}
			}else if(player1.getA()>bp.length){
				jf.setCenter("<htmlInvalid number!</html");
			}
		
	}

	public void bpCheck(final Frame jf) {

		jf.setCenter("<html>" + "Backpack slot 1 = "
				+ whatitem.lookupitem(bp[0]) + "<br></br>"
				+ "Backpack slot 2 = " + whatitem.lookupitem(bp[1])
				+ "<br></br>" + "Backpack slot 3 = "
				+ whatitem.lookupitem(bp[2]) + "<br></br>"
				+ "Backpack slot 4 = " + whatitem.lookupitem(bp[3])
				+ "<br></br>" + "Backpack slot 5 = "
				+ whatitem.lookupitem(bp[4]) + "<br></br>"
				+ "Backpack slot 6 = " + whatitem.lookupitem(bp[5])
				+ "<br></br>" + "Backpack slot 7 = "
				+ whatitem.lookupitem(bp[6]) + "<br></br>"
				+ "Backpack slot 8 = " + whatitem.lookupitem(bp[7])
				+ "<br></br>" + "Backpack slot 9 = "
				+ whatitem.lookupitem(bp[8]) + "<br></br>"
				+ "Backpack slot 10 = " + whatitem.lookupitem(bp[9]));
	}

	public void addItem(int item) {
		int check = 0;
		for (int i = 0; i < bp.length; i++) {
			check = 1;
			if (bp[i] == 0 && item > 1) {
				bp[i] = item;
				check = 2;
				break;
			}
			if (item <= 1) {
				check = 0;
			}

		}
		if (check == 1) {
			JOptionPane.showMessageDialog(null, "Your inventory is full!");
		} else if (check == 2) {
			JOptionPane.showMessageDialog(null,
					"You added " + whatitem.lookupitem(item)
							+ " to your inventory!");
		}

	}

	public int getBp0() {
		return bp[0];
	}

	public int getBp1() {
		return bp[1];
	}

	public int getBp2() {
		return bp[2];
	}

	public int getBp3() {
		return bp[3];
	}

	public int getBp4() {
		return bp[4];
	}

	public int getBp5() {
		return bp[5];
	}

	public int getBp6() {
		return bp[6];
	}

	public int getBp7() {
		return bp[7];
	}

	public int getBp8() {
		return bp[8];
	}

	public int getBp9() {
		return bp[9];
	}

	public void setBp0(int bp) {
		this.bp[0] = bp;
	}

	public void setBp1(int bp) {
		this.bp[1] = bp;
	}

	public void setBp2(int bp) {
		this.bp[2] = bp;
	}

	public void setBp3(int bp) {
		this.bp[3] = bp;
	}

	public void setBp4(int bp) {
		this.bp[4] = bp;
	}

	public void setBp5(int bp) {
		this.bp[5] = bp;
	}

	public void setBp6(int bp) {
		this.bp[6] = bp;
	}

	public void setBp7(int bp) {
		this.bp[7] = bp;
	}

	public void setBp8(int bp) {
		this.bp[8] = bp;
	}

	public void setBp9(int bp) {
		this.bp[9] = bp;
	}

}
