import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import javax.swing.JOptionPane;

public class Battle {

	private JButton attack = new JButton("Attack");
	private JButton potion = new JButton("Use potion");
	private JButton flee = new JButton("Flee");
	Dead d = new Dead();
	Loot l = new Loot();
	Item i = new Item();
	public void battle(final Player player1,final Monster monster1 ,final Frame jf, final Backpack bag){
		jf.reset();

		if (monster1.getName() == "Orc") {
			jf.setEast("orc.png");
		} else if (monster1.getName() == "Troll") {
			jf.setEast("orcskadad.png");
		} else if (monster1.getName() == "Elf") {
			jf.setEast("orcskadad.png");
		} else if (monster1.getName() == "Demon") {
			jf.setEast("orcskadad.png");
		}

		jf.setWest("player.png");

		jf.setCenter("<html>"
				+ player1.getName()
				+ " Hp: "
				+ player1.getHp()
				+ "/"
				+ player1.getmaxHp()
				+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
				+ monster1.getName() + " Hp: " + monster1.getHp() + "/"
				+ monster1.getmaxHp() + "<br></br>" + player1.getName()
				+ " Attack: "
				+ ((player1.getStr() + i.itemdmg(player1.getItem())) + 6)
				+ " - "
				+ ((player1.getStr() + i.itemdmg(player1.getItem())) + 11)
				+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
				+ monster1.getName() + " Attack: " + (monster1.getStr() + 6)
				+ " - " + (monster1.getStr() + 11) + "<br></br>"
				+ player1.getName() + " have " + player1.getPotion()
				+ "/4 potion." + "</html>");
		jf.addP(attack);
		jf.addP(potion);
		jf.addP(flee);

		attack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				int x, y;
				x = player1.attack();
				y = monster1.attack();
				jf.setTop("<html>" +player1.getName() + " attack and hit's "
						+ monster1.getName() + " for " + x + "<br></br>"
						+ monster1.getName() + " hit's back for " + y+ "<html>");
				monster1.setHp(monster1.getHp() - x);
				if (monster1.getHp() < (monster1.getmaxHp() / 2)) {
					if (monster1.getName() == "Orc") {
						jf.setEast("orcskadad.png");
					} else if (monster1.getName() == "Troll") {
						jf.setEast("orcskadad.png");
					} else if (monster1.getName() == "Elf") {
						jf.setEast("orcskadad.png");
					} else if (monster1.getName() == "Demon") {
						jf.setEast("orcskadad.png");
					}
				}
				if (monster1.getHp() > 0) {
					player1.setHp(player1.getHp() - y);
					jf.setCenter("<html>"
							+ player1.getName()
							+ " Hp: "
							+ player1.getHp()
							+ "/"
							+ player1.getmaxHp()
							+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
							+ monster1.getName()
							+ " Hp: "
							+ monster1.getHp()
							+ "/"
							+ monster1.getmaxHp()
							+ "<br></br>"
							+ player1.getName()
							+ " Attack: "
							+ ((player1.getStr() + i.itemdmg(player1.getItem())) + 6)
							+ " - "
							+ ((player1.getStr() + i.itemdmg(player1.getItem())) + 11)
							+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
							+ monster1.getName() + " Attack: "
							+ (monster1.getStr() + 6) + " - "
							+ (monster1.getStr() + 11) + "<br></br>"
							+ player1.getName() + " have "
							+ player1.getPotion() + "/4 potion." + "</html>");
				} else {
					player1.setExp(player1.getExp() + monster1.getExp());
					jf.setCenter("Congratulations");
					jf.setTop(player1.getName() + " defeated the "
							+ monster1.getName() + "\n" + player1.getName()
							+ " gain " + monster1.getExp() + " experience!");
					if (player1.getExp() > player1.getnextLevel()) {
						JOptionPane
								.showMessageDialog(
										null,
										"Congratulations you have advanced 1 level!"
												+ "\n"
												+ player1.getName()
												+ " have gain +25 max hp and +3 attack!\n\n");
						player1.setLevel(player1.getLevel() + 1);
						player1.setStr(player1.getStr() + 3);
						player1.setmaxHp(player1.getmaxHp() + 25);
						player1.setExp(0);
						player1.setnextLevel(player1.getnextLevel() + 35);
					}
					int item = l.getItem(player1);
					int gold = l.getGold();
					JOptionPane.showMessageDialog(null, player1.getName()
							+ " Looted " + i.lookupitem(item) + " and " + gold
							+ " gold!");
					player1.setGold(player1.getGold()+gold);
					jf.setCenter("   ");
					jf.setTop(" ");

					if (item <= player1.getItem() ||item <=1) {
						bag.addItem(item);
						jf.reset();
						MainMenu m = new MainMenu();
						m.StartMenu(player1, jf,bag);
					} else {
						l.useItem(player1, item,bag);
						jf.reset();
						MainMenu m = new MainMenu();
						m.StartMenu(player1, jf,bag);
					}

				}

				d.die(player1);

			}
		});
		potion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (player1.getPotion() <= 0) {
					jf.setTop("You dont have any potions");
				} else {

					player1.setPotion(player1.getPotion() - 1);
					int x = player1.getmaxHp() / 4;
					jf.setTop(player1.getName() + " restor " + x + " hp!");
					player1.setHp(player1.getHp() + x);
					if (player1.getHp() > player1.getmaxHp()) {
						player1.setHp(player1.getmaxHp());
					}
					jf.setCenter("<html>"
							+ player1.getName()
							+ " Hp: "
							+ player1.getHp()
							+ "/"
							+ player1.getmaxHp()
							+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
							+ monster1.getName()
							+ " Hp: "
							+ monster1.getHp()
							+ "/"
							+ monster1.getmaxHp()
							+ "<br></br>"
							+ player1.getName()
							+ " Attack: "
							+ ((player1.getStr() + i.itemdmg(player1.getItem())) + 6)
							+ " - "
							+ ((player1.getStr() + i.itemdmg(player1.getItem())) + 11)
							+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
							+ monster1.getName() + " Attack: "
							+ (monster1.getStr() + 6) + " - "
							+ (monster1.getStr() + 11) + "<br></br>"
							+ player1.getName() + " have "
							+ player1.getPotion() + "/4 potion." + "</html>");
				}
			}
		});
		flee.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MainMenu m = new MainMenu();
				m.StartMenu(player1, jf,bag);
			}
		});
	}
}
