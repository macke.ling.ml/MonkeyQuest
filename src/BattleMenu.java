import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class BattleMenu {

	private JButton orc = new JButton("Orc");
	private JButton troll = new JButton("Troll");
	private JButton elf = new JButton("Elf");
	private JButton demon = new JButton("Demon");
	private JButton goback = new JButton("Go Back");

	public void choosebattle(final Player player1, final Frame jf,final Backpack bag){
		jf.setEast("test.png");
		jf.setWest("player.png");
		jf.setTop("What do you like to kill " + player1.getName() + "?");
		jf.setCenter("<html>" + player1.getName() + " Hp: " + player1.getHp()
				+ "/" + player1.getmaxHp() + "<br></br>" + player1.getName()
				+ " have " + player1.getPotion() + "/4 potion." + "</html>");
		jf.addP(orc);
		jf.addP(troll);
		jf.addP(elf);
		jf.addP(demon);
		jf.addP(goback);

		orc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jf.reset();
				Monster morc = new Monster("Orc", 75, 7, 40);
				Battle b = new Battle();
				b.battle(player1, morc, jf,bag);
			}
		});
		troll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jf.reset();
				Monster mtroll = new Monster("Troll", 100, 11, 50);
				Battle b = new Battle();
				b.battle(player1, mtroll, jf, bag);
			}
		});
		elf.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jf.reset();
				Monster melf = new Monster("Elf", 150, 16, 75);
				Battle b = new Battle();
				b.battle(player1, melf, jf, bag);
			}
		});
		demon.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jf.reset();
				Monster mdemon = new Monster("Demon", 250, 30, 175);
				Battle b = new Battle();
				b.battle(player1, mdemon, jf, bag);
			}
		});
		goback.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MainMenu m = new MainMenu();
				m.StartMenu(player1, jf, bag);
			}
		});

	}

}
