import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JButton;

public class MainMenu {
	String name;
	private JButton hunt = new JButton("Go hunting");
	private JButton rest = new JButton("Rest");
	private JButton bp = new JButton("Backpack");
	private JButton stats = new JButton("Stats");
	private JButton save = new JButton("Save");
	private JButton load = new JButton("Load");

	SaveLoad sv = new SaveLoad();
	Item i = new Item();
	public void StartMenu(final Player player1, final Frame jf, final Backpack bag) {
		jf.reset();
		jf.setEast("test.png");
		jf.setWest("player.png");
		jf.setTop("Welcome " + player1.getName());
		jf.setCenter("<html>" + player1.getName() + " Hp: " + player1.getHp()
				+ "/" + player1.getmaxHp() + "<br></br>" + player1.getName()
				+ " have " + player1.getPotion() + "/4 potion." + "</html>");
		stats.setText(player1.getName() + " stats");
		jf.addP(hunt);
		jf.addP(rest);
		jf.addP(bp);
		jf.addP(stats);
		jf.addP(save);
		jf.addP(load);

		hunt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jf.reset();
				BattleMenu battle = new BattleMenu();
				battle.choosebattle(player1, jf, bag);

			}
		});
		rest.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jf.reset();
				Rest sleep=new Rest();
				sleep.playerRest(player1, jf, bag);
			}
		});
		
		bp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jf.reset();
				BackpackMenu bpm= new BackpackMenu();
				bpm.inventory(player1, jf, bag);
			}
		});
		
		stats.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				jf.setCenter("<html>"
						+ player1.getName()
						+ " stats are<br></br>Attack: "
						+ ((player1.getStr() + i.itemdmg(player1.getItem())) + 6)
						+ " - "
						+ ((player1.getStr() + i.itemdmg(player1.getItem())) + 11)
						+ "<br></br>Max hp: " + player1.getmaxHp()
						+ "<br></br>Level: " + player1.getLevel()
						+ "<br></br>Expeciance: " + player1.getExp() + "/"
						+ player1.getnextLevel() + "<br></br>Gold: "
						+ player1.getGold() + "<br></br>Weapon using: "
						+ i.lookupitem(player1.getItem()) + "</html>");
			}
		});
		save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {

					sv.save(player1,bag);
					jf.setTop("You have saved your player!");
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		load.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {

					sv.load(player1,bag);
					jf.setTop("Welcome " + player1.getName());
					jf.setCenter("<html>" + player1.getName() + " Hp: "
							+ player1.getHp() + "/" + player1.getmaxHp()
							+ "<br></br>" + player1.getName() + " have "
							+ player1.getPotion() + "/4 potion." + "</html>");
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});

	}

	public String getname() {
		return name;
	}

}
