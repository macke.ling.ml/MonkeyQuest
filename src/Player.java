import java.util.Random;

public class Player {
	private int question=0;
	private int hp;
	private int str;
	private int level;
	private int exp;
	private int maxHp;
	private int nextLevel;
	private int potion;
	private String name;
	private int gold;
	private int itemid;

	public Player(String name) {
		hp = 100;
		str = 10;
		level = 1;
		exp = 0;
		maxHp = 100;
		nextLevel = 100;
		potion = 1;
		gold = 0;
		this.name = name;
		itemid = 0;
	}

	public int getHp() {
		return hp;
	}

	public int getStr() {
		return str;
	}

	public int getnextLevel() {
		return nextLevel;
	}

	public int getLevel() {
		return level;
	}

	public int getExp() {
		return exp;
	}

	public int getmaxHp() {

		return maxHp;
	}

	public int getPotion() {

		return potion;
	}

	public int getGold() {

		return gold;
	}

	public int getItem() {
		return itemid;
	}

	public String getName() {
		return name;
	}
	public int getA(){
		return question;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public void setStr(int str) {
		this.str = str;
	}

	public void setnextLevel(int nextLevel) {
		this.nextLevel = nextLevel;
	}

	public void setExp(int exp) {
		this.exp = exp;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public void setmaxHp(int maxHp) {

		this.maxHp = maxHp;
	}

	public void setPotion(int potion) {

		if (potion < 5) {
			this.potion = potion;
		}

	}

	public void setName(String name) {
		this.name = name;
	}
	public void setA(int a){
		question=a;
	}


	public void setGold(int gold) {
		this.gold = gold;
	}

	public void setItem(int itemid) {
			this.itemid = itemid;
	}

	public int attack() {
		Item i = new Item();
		int x, itemdmg;
		itemdmg = i.itemdmg(itemid);
		Random r = new Random();
		x = r.nextInt(5) + 7;
		x = x + (str + itemdmg);
		return x;
	}
}
