import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class Begin {
	private JButton bt1 = new JButton("OK");
	private JButton bt2 = new JButton("Quit");
	private JTextField name = new JTextField(5);
	private JPanel button = new JPanel();
	private JLabel toptext = new JLabel();
	private JLabel west = new JLabel();
	private JLabel east = new JLabel();
	private JFrame jfr = new JFrame();

	public Begin() {

		jfr.setTitle("Quest of the monkey");
		jfr.setVisible(true);
		toptext.setHorizontalAlignment(SwingConstants.CENTER);
		name.setHorizontalAlignment(SwingConstants.CENTER);
		jfr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jfr.add(toptext, BorderLayout.NORTH);
		jfr.add(name, BorderLayout.CENTER);
		jfr.add(button, BorderLayout.SOUTH);
		jfr.add(west, BorderLayout.WEST);
		jfr.add(east, BorderLayout.EAST);
		jfr.setSize(300, 200);
		jfr.setLocation(650, 400);
		button.add(bt1);
		button.add(bt2);
		toptext.setText("<html><br></br>Welcome to Quest of the monkey!<br></br>What will you name be?<br><br><br></html>");
		west.setText("          ");
		east.setText("          ");
		bt1.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				Backpack bag=new Backpack();
				Frame jf = new Frame();
				String n = name.getText();
				Player player1 = new Player(n);
				MainMenu m = new MainMenu();
				m.StartMenu(player1, jf, bag);
				jfr.dispose();
			}

		});

		bt2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
	}

	public void start() {
		// start metod

	}

}
