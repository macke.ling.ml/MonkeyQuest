import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class Qframe {
	private JFrame jf = new JFrame();
	private JPanel button = new JPanel();
	private JLabel toptext = new JLabel("");
	private JLabel centertext = new JLabel("");
	private JLabel west = new JLabel();
	private JLabel east = new JLabel();
	private JButton bt1 = new JButton("OK");
	private JButton bt2 = new JButton("Quit");
	private JTextField name = new JTextField(5);
	public void frame(final Player player1,final Frame jfr,final Backpack bag,final int option) {
		jf.setTitle("Quest of the monkey");
		jf.setVisible(true);
		toptext.setHorizontalAlignment(SwingConstants.CENTER);
		name.setHorizontalAlignment(SwingConstants.CENTER);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.add(toptext, BorderLayout.NORTH);
		jf.add(name, BorderLayout.CENTER);
		jf.add(button, BorderLayout.SOUTH);
		jf.add(west, BorderLayout.WEST);
		jf.add(east, BorderLayout.EAST);
		jf.setSize(300, 200);
		jf.setLocation(650, 400);
		button.add(bt1);
		button.add(bt2);
		toptext.setText("");
		west.setText("          ");
		east.setText("          ");
		
		bt1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(option==1){
				int num= Integer.parseInt(name.getText());
				player1.setA(num-1);
				bag.changewep(player1, jfr);
				}else if(option==2){
				int num= Integer.parseInt(name.getText());
				player1.setA(num-1);
				bag.bp[player1.getA()]=0;
				bag.bpCheck(jfr);
				}
				jf.dispose();
			}

		});

		bt2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jf.dispose();
			}
		});
	}

	public void setsize(int x, int y) {
		jf.setSize(x, y);
	}

	public void setlocation(int x, int y) {
		jf.setLocation(x, y);
	}

	public void setTop(String x) {
		toptext.setText(x);
	}

	public void setCenter(String x) {
		centertext.setText(x);
	}

	public void setWest(String x) {
		west.setIcon(new javax.swing.ImageIcon(getClass().getResource(x)));
	}

	public void setEast(String x) {
		east.setIcon(new javax.swing.ImageIcon(getClass().getResource(x)));
	}

	public void addP(JButton x) {
		button.add(x);
	}

	public void addn(JTextField name) {
		jf.add(name, BorderLayout.CENTER);
	}

	public void setName(JTextField name) {
		jf.add(name, BorderLayout.CENTER);
	}

	// public String getName(){
	// return name.getText();
	// }

	public void reset() {
		button.removeAll();
		button.revalidate();
		button.repaint();
		centertext.setText("   ");
		toptext.setText("  ");
	}

}
